"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from __future__ import annotations

import inspect
import json
from dataclasses import asdict
from typing import *

from pydantic.dataclasses import dataclass

__VERSION__ = 0x000100D


class ConfigLoadError(Exception):
    pass


class IllegalOperation(Exception):
    pass


if TYPE_CHECKING:
    # fake base class to make IDEs and type checkers happy
    class _IDETypeCheckFix:
        def __init__(self, self2=None, **kwargs):
            del self2, kwargs

        def __setattr__(self, key, value):
            ...


def config_section(identifier: str) -> Callable[[Type["_IDETypeCheckFix"]], Type["_IDETypeCheckFix"]]:
    """
    Decorator to turn a class in to a config section dataclass.
    It adds the following methods:
    * `__init__` which gets the config from the cache, and copies it into the config object

    * `__repr__` which gives something like this: `Config(foo=12, eggs="spam", ...)`, recursively stepping in to
        contained dataclasses (this is the same as a vanilla Python dataclass)

    * `__eq__` which simply checks equality on all attributes (this is the same as a vanilla Python dataclass)

    * `__setattr__` which checks you're not using any illegal types
        (you can only use str, int, float, list, dict, bool, None)

    * `update` which re-copies the config from the Config object's cache. Note that this does not actually reload
        the file - this must be done manually using the `load` method on the Config object itself (not the config
        dataclass) (this method is not added when `frozen` is True)

    :param identifier: string to identify this section in a config file
    :return: decorated config section dataclass

    Example usage:
    >>> @config_section()
    ... class MyConfigSection:
    ...     value: str
    ...     value_with_default: bool = True
    ...     you_can_use_any_combination_of_the_basic_types: Dict[str, List[[Union[int, float]]]]
    ...     but_not_functions: Callable  # will cause strange unexpected behaviour
    ...     nor_anything_else_strange: Set[bytes, ...]  # also not ok
    ...
    >>> with open("my_config.json") as f:
    ...     my_config = JSONConfig(f)
    >>> my_config_section = MyConfigSection(my_config)
    >>> print(my_config_section.value)
    "hello world"
    """

    def decorator(cls: Type["_IDETypeCheckFix"]):
        cls: Type["_IDETypeCheckFix"] = dataclass(cls)
        super_init = cls.__init__

        def __init__(self, config):
            if config.full_config is None:
                import warnings
                warnings.warn("Config has not yet been loaded. Load it with load_config_file(path)")
                return
            kwargs = config.full_config.get(identifier) or {}
            # get all the parameters
            parameters = dict(inspect.signature(super_init).parameters)
            # remove the special `self` parameter
            del parameters['self']
            # parameters
            # which parameters do not have default values?
            required = {k for k, v in parameters.items() if v.default is v.empty}
            # are any parameters required but not specified?
            missing = set(required) - set(kwargs.keys())
            if missing:
                # error!
                raise ConfigLoadError(f"Missing required configuration values: {', '.join(map(str, missing))}")

            try:
                super_init(self, **kwargs)
            except ValueError as e:
                raise ConfigLoadError(*e.args)

            # suppress error for if we are reinitialising (via the `update` method)
            # (because these values won't have changed)
            try:
                self.config = config
                self._writable = config.writable
                self._allow_delete = config.allow_delete
                self.__setattr__ = __setattr__
            except IllegalOperation:
                pass

        def __delattr__(self, key):
            if self._writable and self._allow_delete:
                object.__delattr__(self, key)
            else:
                raise IllegalOperation(f"Cannot delete attribute {key}")
    
        def __setattr__(self, key, value):
            if self._writable and value in vars(self):
                cls.__setattr__(self, key, value)
                self.config.full_config[identifier] = asdict(self)
            raise IllegalOperation(f"Cannot set attribute {key}")

        def update(self):
            # this just calls __init__ again on the object
            # (which you're not technically supposed to do in python)
            self.__init__()

        # we can't just extend the class because it would mess up the MRO, class hierarchy, and special attributes
        #  so we just set the new attributes on to the class individually. (curse you, multiple inheritance!)
        cls.__init__ = __init__
        cls.__delattr__ = __delattr__
        cls.update = update
        return cls

    return decorator


class BaseConfig:
    """
    A class for storing and loading various configuration sections from one file.
    Each section has its own dataclass
    """

    def __init__(self, f: TextIO, writable: bool = False, allow_delete: bool = True):
        self.full_config = self._load(f)
        self._f = f
        self.writable = writable
        self.allow_delete = allow_delete

    def reload(self, f: TextIO):
        self.full_config = self._load(f)

    def resave(self, data):
        self._save(self._f, data)

    def _load(self, f: TextIO) -> Mapping[str, Mapping[str, Any]]:
        """
        load the config from a file-like object and return it
        :param f: file-like object
        :return: config as a mapping of section names to dicts of config keys to values
        """
        return NotImplemented
    
    def _save(self, f: TextIO, data: Mapping[str, Mapping[str, Any]]):
        """
        save the config from a file-like object and return it
        :param f: file-like object
        :return: config as a mapping of section names to dicts of config keys to values
        """
        return NotImplemented


class JSONConfig(BaseConfig):
    """
    loads config in JSON format, where top-level keys to objects represent sections
    E.G.
    ```json
        {
            "section1": {...},
            ...
        }
    ```
    """

    def _load(self, f):
        return json.load(f)

    def _save(self, f, data):
        json.dump(f, data)
