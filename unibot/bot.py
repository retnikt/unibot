"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import asyncio
import logging
import shlex
import sys
import time
from platform import platform
from typing import *

import discord

import unibot._utils
import unibot.config
import unibot.parser
import unibot.plugin_manager
from unibot import command
from . import _globals

__VERSION__ = 0x000100d0

EVENT_NAMES = [
    "connect",
    "disconnect",
    "ready",
    "shard_ready",
    "resumed",
    "error",
    "socket_raw_receive",
    "socket_raw_send",
    "typing",
    "message",
    "message_delete",
    "bulk_message_delete",
    "raw_message_delete",
    "raw_bulk_message_delete",
    "message_edit",
    "raw_message_edit",
    "reaction_add",
    "raw_reaction_add",
    "reaction_remove",
    "raw_reaction_remove",
    "reaction_clear",
    "raw_reaction_clear",
    "private_channel_delete",
    "private_channel_create",
    "private_channel_update",
    "private_channel_pins_update",
    "guild_channel_delete",
    "guild_channel_create",
    "guild_channel_update",
    "guild_channel_pins_update",
    "guild_integrations_update",
    "webhooks_update",
    "member_join",
    "member_remove",
    "member_update",
    "user_update",
    "guild_join",
    "guild_remove",
    "guild_update",
    "guild_role_create",
    "guild_role_delete",
    "guild_role_update",
    "guild_emojis_update",
    "guild_available",
    "guild_unavailable",
    "voice_state_update",
    "member_ban",
    "member_unban",
    "group_join",
    "group_remove",
    "relationship_add",
    "relationship_remove",
    "relationship_update",
]


@unibot.config.config_section("core")
class CoreConfig:
    prefix: str = "~"
    debug: bool = False
    debug_channel: Optional[str] = None
    load_base: bool = True
    safe_mode: bool = False
    reconnect: bool = True


@unibot.config.config_section("credentials")
class Credentials:
    client_id: str
    bot_token: str


class Listener:
    def __init__(self, func: Callable[..., Coroutine], timeout: Optional[int] = None,
                 check: Optional[Callable[..., Coroutine]] = None,
                 on_timeout: Optional[Callable[..., Coroutine]] = None):
        self.func = func
        self.timeout = timeout
        self._last_time = time.time()
        self.on_timeout = on_timeout
        self.check = check

    def is_timed_out(self):
        return time.time() > self._last_time + self.timeout

    async def __call__(self, *args, **kwargs):
        self._last_time = time.time()
        await self.func(*args, **kwargs)


class Bot(discord.Client):
    FORMAT = "[ {levelname:<7} ] [ {name:<20} ]  {message}"

    def __init__(self, logger: "logging.Logger", config_file: TextIO, credentials_file: TextIO):
        super(Bot, self).__init__()

        self.config_file = config_file
        self.config_config = unibot.config.JSONConfig(config_file)
        self.config = CoreConfig(self.config_config)

        self.credentials_file = config_file
        self.config_credentials = unibot.config.JSONConfig(credentials_file)
        self.credentials = Credentials(self.config_credentials)

        self._listener_coros = {event_name: [] for event_name in EVENT_NAMES}
        self.commands = []

        self.logger = logger

        self.plugin_manager = unibot.plugin_manager.PluginManager(self)

        self.root_parser = unibot.parser.UnibotParser()
        self.subcommands_class = command.CommandWithSubCommands.new("root")
        self.subcommands: Optional[command.CommandWithSubCommands] = None

        self._planned_disconnect = False

        self.plugin_unload_hooks = [self._recursively_remove_commands]

        self.global_bot_context = self._GlobalBotContext(self)

        @self.event_listener("message")
        async def on_message(message):
            if message.author == self.user or not message.content.startswith(self.config.prefix):
                return
            # remove prefix
            content = message.content[len(self.config.prefix):]
            args = shlex.split(content)
            self.root_parser.context_message = message
            try:
                namespace = self.root_parser.parse_args(args)
            except unibot.parser.CommandError:
                return
            self.logger.debug(f"Executing command: '{message.content}'")
            # noinspection PyBroadException
            try:
                await self.subcommands(message, **vars(namespace))
            except Exception:
                self.logger.error(f"Exception in command '{message.content}'", exc_info=True)
                if self.config.debug:
                    import traceback
                    tb = traceback.format_exc()
                    await message.channel.send(f"Error:\n```{tb}```")
                else:
                    await message.channel.send("Oops! An error occurred while executing your command."
                                               "Please contact the server administrator."
                                               "If you are the server administrator, check the server logs" +
                                               (f" and debug channel ({self.config.debug_channel})"
                                                if self.config.debug_channel else "") +
                                               " for more details.")
            finally:
                self.root_parser.context_message = None

        @self.event_listener("ready")
        async def on_ready():
            self.logger.info("Ready.")

        @self.event_listener("disconnect")
        async def on_disconnect():
            if self._planned_disconnect:
                self.logger.info("Disconnected.")
            else:
                self.logger.error("Disconnected.")

    async def close(self):
        self.logger.info("Logging out")
        self._planned_disconnect = True
        return super(Bot, self).close()

    def event(self, coro):
        raise NotImplementedError("the event method is not supported on a Bot. "
                                  "Use the @event_listener decorator instead")

    def event_listener(self, *names: str, timeout: Optional[int] = None,
                       check: Optional[Callable[..., Coroutine]] = None,
                       on_timeout: Optional[Callable[..., Coroutine]] = None):
        """
        decorator which registers an event listener
        :param names: event names to listen for
        :param timeout: seconds to time out after, or None for no timeout
        :param check: check to perform before execution of callback and reset of timeout
        :param on_timeout: callback for when the timeout is reached.
         Note that this is not necessarily second-accurate - the listener is only removed
         and on_timeout called on the next event received after the timeout.
        :return:
        """

        def decorator(coro: Callable[..., Coroutine]):
            listener = Listener(coro, timeout, check, on_timeout)
            for name in names:
                if name not in EVENT_NAMES:
                    # cleanup
                    del listener
                    raise ValueError(f"no such event '{name}'")
                self._listener_coros[name].append(listener)
            return coro

        return decorator

    def dispatch(self, event, *args, **kwargs):
        for i, listener in enumerate(self._listener_coros.get(event, [])):
            try:
                if listener.is_timed_out():
                    if listener.on_timeout:
                        await listener.on_timeout(*args, **kwargs)
                    del self._listener_coros[event][i]
                    continue
                if listener.check and not await listener.check(*args, **kwargs):
                    continue
                asyncio.create_task(listener.call(*args, **kwargs))
            except Exception as e:
                self.logger.error(f"Exception while dispatching event {event}", exc_info=e)
                continue
        return super(Bot, self).dispatch(event, *args, **kwargs)

    def plugin_unload_hook(self, fn):
        self.plugin_unload_hooks.append(fn)

    def _recursively_remove_commands(self, plugin, subcommands=None):
        subcommands = subcommands or self.subcommands
        for i, cmd in enumerate(subcommands.commands):
            if cmd.__module__ is plugin:
                del subcommands.commands[i]
            elif isinstance(command, unibot.command.CommandWithSubCommands):
                self._recursively_remove_commands(plugin, cmd)

    def command(self, cls):
        return self.subcommands_class.command(cls)

    def generate_add_url(self):
        return f"https://discordapp.com/oauth2/authorize?&client_id={self.credentials.client_id}" \
               "&scope=bot&permissions=8"

    def open_add_url(self):
        import webbrowser
        webbrowser.open(self.generate_add_url())

    class _GlobalBotContext:
        def __init__(self, bot):
            self.bot = bot

        def __enter__(self):
            _globals.bot = self.bot
            return self.bot

        def __exit__(self, exc_type, exc_val, exc_tb):
            _globals.bot = None

    def run(self):
        with self.global_bot_context:
            self.logger.info(f"Starting bot.")
            self.logger.debug(f"Unibot version: {__VERSION__:08x}")
            self.logger.debug(f"Python version: {sys.hexversion:08x}")
            self.logger.debug(f"Platform: {platform()}")
            self.logger.info("Loading config.")
            if self.config.load_base:
                self.logger.info(f"Loading base.")
                from unibot import base
                self.plugin_manager.plugins["unibot.base"] = base
            if self.config.safe_mode:
                self.logger.info(f"Skipping loading plugins (safe mode enabled).")
                return
            else:
                self.logger.info("Loading plugins.")
                self.plugin_manager.load_plugins()
            # wtf, IDEA!?
            # noinspection PyArgumentList
            self.subcommands = self.subcommands_class(self.root_parser)
            self.logger.info("Logging in.")

            loop = asyncio.get_event_loop()
            if sys.version_info < (3, 7, 4):
                # a bug between python 3.7 and 3.7.3 causes some weird SSL error which causes crashes (see docstring)
                unibot._utils.ignore_aiohttp_ssl_error(loop)
            try:
                loop.run_until_complete(self.start(self.credentials.bot_token, reconnect=self.config.reconnect))
            except KeyboardInterrupt or SystemExit:
                loop.run_until_complete(self.logout())
            # cancel all tasks lingering
            finally:
                self.logger.info("Shutting down.")
                loop.close()
