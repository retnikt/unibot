"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import asyncio
import importlib.machinery
import importlib.util
import json
import logging
import pathlib
import pkgutil
import types
from typing import *

from pydantic import BaseConfig
from pydantic.dataclasses import dataclass

import unibot.config

__VERSION__ = 0x000100D


class Config(BaseConfig):
    arbitrary_types_allowed = True


@dataclass(config=Config)
class PluginManifest:
    name: str
    version: str
    description: Optional[str] = None
    author: Optional[str] = None
    license: Optional[str] = None
    docs: Optional[str] = None
    source: Optional[str] = None
    issues: Optional[str] = None
    requirements: List[Mapping[str, str]] = ()


class PluginNotFound(Exception):
    pass


@unibot.config.config_section("plugins")
class PluginsConfig:
    plugin_search_directories: List[str] = ("plugins",)
    plugins_enabled: List[str] = ()
    plugin_unload_timeout: Union[float, int] = 5


class PluginManager:
    PLUGIN_NAME = "unibot._loaded_plugin_{name}"
    PLUGIN_MANIFEST_FILENAME = "unibot_plugin_manifest.json"

    def __init__(self, bot):
        self.bot = bot
        self.plugins = {}
        self.logger = logging.getLogger("unibot.plugins")
        self.config = PluginsConfig(self.bot.config_config)

    def _get_manifest_from_plugin_path(self, path: pathlib.Path):
        manifest_path = path / self.PLUGIN_MANIFEST_FILENAME
        with manifest_path.open() as f:
            raw = json.load(f)
        manifest = PluginManifest(**raw)
        return manifest

    def load_plugins(self):
        for finder, name, _ in pkgutil.iter_modules(self.config.plugin_search_directories):
            spec = finder.find_spec(name)
            if spec is None:
                self.logger.debug(f"Ignoring plugin {name} with no module spec available")
                continue
            try:
                manifest = self._get_manifest_from_plugin_path(pathlib.Path(spec.origin))
            except OSError or TypeError or ValueError as e:
                self.logger.warning(f"Skipping plugin in '{spec.origin}' with invalid or inaccessible manifest file.",
                                    exc_info=e)
                continue
            else:
                if name in self.plugins:
                    self.logger.warning(f"Skipping plugin with duplicate name '{name}' from '{spec.origin}'.")
                self.load_plugin_from_spec(spec, manifest)

    def find_plugin_manifest(self, target):
        for finder, name, _ in pkgutil.iter_modules(self.config.plugin_search_directories):
            if name == target:
                spec = finder.find_spec(name)
                try:
                    manifest = self._get_manifest_from_plugin_path(pathlib.Path(spec.origin))
                except OSError or TypeError or ValueError as e:
                    self.logger.warning(f"Skipping plugin in '{spec.origin}' with malformed or inaccessible manifest.",
                                        exc_info=e)
                    continue
                else:
                    return manifest
        raise PluginNotFound(f"Plugin {target} could not be found")

    def _exc_wrapper(self, plugin_name, hook_name, coro):
        try:
            await coro
        except Exception as e:
            self.logger.error(f"Exception in plugin unload hook {hook_name} for plugin {plugin_name}", exc_info=e)

    async def unload_plugin(self, name):
        plugin = self.plugins.get(name)
        if plugin is None:
            raise NameError("Plugin is not loaded")

        if hasattr(plugin, 'unload_hook'):
            try:
                if asyncio.iscoroutinefunction(plugin.unload_hook):
                    # the plugin's own unload hook must be executed synchronously
                    await asyncio.wait_for(plugin.unload_hook(plugin), self.config.plugin_unload_timeout)
                elif callable(plugin.unload_hook):
                    plugin.unload_hook(plugin)
            except Exception as e:
                self.logger.error(f"Exception in plugin unload hook for plugin {name}:",
                                  exc_info=e)

        coros = []
        for hook in self.bot.plugin_unload_hooks:
            if asyncio.iscoroutinefunction(hook):
                coros.append(self._exc_wrapper(name, hook.__name__, hook(plugin)))
            else:
                try:
                    hook(plugin)
                except Exception as e:
                    self.logger.error(f"Exception in plugin unload hook {hook.__name__} for plugin {name}:",
                                      exc_info=e)

    async def reload_plugin(self, name):
        await self.unload_plugin(name)
        path = pathlib.Path(self.plugins[name].__file__)
        importlib.reload(self.plugins[name])
        self.load_plugin_from_path(path.parent)

    async def reload_all_plugins(self):
        for name in self.plugins:
            await self.unload_plugin(name)
        for name, plugin in self.plugins.items():
            path = pathlib.Path(plugin.__file__)
            importlib.reload(plugin)
            self.load_plugin_from_path(path.parent)

    def load_plugin_from_path(self, path: Union[str, pathlib.Path]):
        spec = importlib.util.spec_from_file_location(str(path))
        manifest = self._get_manifest_from_plugin_path(path)
        return self.load_plugin_from_spec(spec, manifest)

    def load_plugin_from_module_name(self, module_name: str, manifest: PluginManifest):
        module = importlib.import_module(module_name)
        return self._add_plugin(module, manifest)

    def load_plugin_from_spec(self, spec: importlib.machinery.ModuleSpec, manifest: PluginManifest):
        module = spec.loader.load_module()
        return self._add_plugin(module, manifest)

    def _add_plugin(self, module: types.ModuleType, manifest: PluginManifest):
        module._manifest = manifest
        self.plugins[manifest.name] = module
        return module
