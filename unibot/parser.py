"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import asyncio
from typing import *

import discord

__VERSION__ = 0x000100D


class CommandError(Exception):
    pass


class UnibotParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        self.context_message: Optional[discord.Message] = None
        kwargs["prog"] = "unibot"
        kwargs["add_help"] = False
        super(UnibotParser, self).__init__(*args, **kwargs)

    def error(self, message):
        self._print_message(message)
        raise CommandError(message)

    def _print_message(self, message, file=None):
        asyncio.create_task(self.context_message.channel.send(message))

    def add_subparsers(self, *args, **kwargs):
        return super(UnibotParser, self).add_subparsers(*args, **kwargs)
