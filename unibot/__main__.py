"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from unibot.bot import Bot
import argparse
from logging import INFO, WARNING, DEBUG, ERROR, FATAL

__VERSION__ = 0x000100D


def main(config_file, credentials_file, log_file, log_level=INFO):
    bot = Bot(config_file, credentials_file)
    bot.logger.setLevel(log_level)
    for f in log_file:
        bot.logger.addHandler()
    bot.run()


parser = argparse.ArgumentParser()
parser.add_argument("--version", "-v", action="version", version=hex(__VERSION__))
subparsers = parser.add_subparsers("command")

run = subparsers.add_parser("run")

log = run.add_argument_group("Logging")
log.add_argument("--debug", "-D", help="set log level to debug", action="store_const", const=DEBUG, dest="log_level")
log.add_argument("--info", "-I", help="set log level to info (default)", action="store_const", const=INFO,
                 dest="log_level")
log.add_argument("--warning", "-W", help="set log level to warnings", action="store_const", const=WARNING,
                 dest="log_level")
log.add_argument("--error", "-E", help="set log level to errors", action="store_const", const=ERROR, dest="log_level")
log.add_argument("--fatal", "-F", help="set log level to fatal errors", action="store_const", const=FATAL,
                 dest="log_level")
log.add_argument("--log-file", "-l", help="file to write logs to", action="append")

config = run.add_argument_group("Configuration")
config.add_argument("--config-file", "-c", action="store", type=argparse.FileType('r+'),
                    help="specify file to read and write config to and from (default: ./config.json)")
config.add_argument("--credentials-file", "-d", action="store", type=argparse.FileType('r'),
                    help="specify file to read credentials from (default: ./credentials.json)")

main(**vars(parser.parse_args()))
