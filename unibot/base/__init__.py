"""
unibot - a universal Discord bot
Copyright © 2019 retnikt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import asyncio
from datetime import datetime

import discord

from unibot import bot
from unibot.command import BaseCommand

__VERSION__ = 0x000100D


@bot.command
class Help(BaseCommand):
    name = "help"
    help = "provides help for the bot's commands"

    async def callback(self, message: "discord.Message"):
        # DM the user help
        if not message.author.dm_channel:
            await message.author.create_dm()
        asyncio.create_task(message.author.dm_channel.send(bot.root_parser.format_help()))


@bot.command
class Restart(BaseCommand):
    name = "restart"

    async def callback(self, message: "discord.Message"):
        import os
        import sys
        os.execl(sys.executable, sys.executable, *sys.argv)


@bot.command
class ReloadBase(BaseCommand):
    name = "reload_base"

    async def callback(self, message):
        bot.plugin_manager.reload_plugin("base")
